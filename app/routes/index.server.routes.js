
module.exports = function(app){
  var index = require('../controllers/index.server.controller');
  //get requests to root path call render from controller
  app.get('/', index.render);
};
