process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var mongoose = require('./config/mongoose'),
  express = require('./config/express'),
  passport = require('./config/passport');

var db = mongoose();
var app = express(db);
var passport = passport();

app.listen(3000);
module.exports = app;

console.log('Server running at localhost:3000');


/*
------------------------------
connect
-----------------------------

var connect = require('connect');
var app = connect();

var logger = function(req, res, next){
  console.log(req.method, req.url);

  next();
};

var helloWorld = function(req, res, next){
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello World');
};

var goodbyeWorld = function(req, res, next){
  res.setHeader('Content-Type', 'text/plain');
  res.end('Goodbye World');
};

app.use(logger);
app.use('/hello', helloWorld);
app.use('/goodbye', goodbyeWorld);

app.listen(3000);
console.log('serevr running on port 3000');
*/
