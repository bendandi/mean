module.exports = function(config) {
  config.set({
    frameworks: ['jasmine'],
    files: ['public/*'],
    reporters: ['progress'],
    broswers: ['PhantomJS'],
    captureTimeout: 60000,
    singleRun: true
  });
};
